import gulp from 'gulp';
import connect from 'gulp-connect';
import sass from 'gulp-sass';
import babel from 'gulp-babel';
import autoprefixer from 'gulp-autoprefixer';
import concat from 'gulp-concat';
import uglify from 'gulp-uglify';
import imagemin from 'gulp-imagemin';


// Path
const paths = {
  build: {
    js: './build/js',
    css: './build/css',
    img: './build/img',
    fonts: './build/fonts'
  },
  watch: {
    js: "./src/js/**/*.js",
    css: "./src/sass/**/*.scss",
    img: "./src/img/**/*"
  },
  scripts: [
    './bower_components/jquery/dist/jquery.min.js',
    './src/js/main.js'
  ],
  styles: [
    './bower_components/normalize-css/normalize.css',
    './src/sass/main.scss'
  ],
  fonts: './src/fonts/**/*',
  images: './src/img/**/*'
};

// Prepare css files
gulp.task('styles', () => {
  return gulp.src(paths.styles)
    .pipe(sass().on('error', sass.logError))
    .pipe(concat('main.min.css'))
    .pipe(autoprefixer())
    .pipe(gulp.dest(paths.build.css));
});

// Prepare js files
gulp.task('scripts', () => {
  return gulp.src(paths.scripts)
    .pipe(babel({
      presets: ['env']
    }))
    .pipe(uglify())
    .pipe(concat('main.min.js'))
    .pipe(gulp.dest(paths.build.js));
});

// Copy all static images
gulp.task('images', () => {
  return gulp.src(paths.images)
    .pipe(imagemin({optimizationLevel: 5}))
    .pipe(gulp.dest(paths.build.img));
});

// Copy fonts
gulp.task('fonts', () => {
  return gulp.src(paths.fonts)
    .pipe(gulp.dest(paths.build.fonts));
});

// Rerun the task when a file changes
gulp.task('watch', () => {
  gulp.watch(paths.watch.js, ['scripts']);
  gulp.watch(paths.watch.img, ['images']);
  gulp.watch(paths.watch.css, ['styles']);
});

// Static server
gulp.task('server', () => {
  connect.server({
    port: 3000,
    livereload: true
  });
});

// The default task
gulp.task('default', ['server', 'watch']);
